package com.sk4atg89.data.di

import com.sk4atg89.data.game.GameRepositoryImpl
import com.sk4atg89.domain.game.repository.GameRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

val dataModules = Kodein.Module {
    import(repositoryModule)

}

private val repositoryModule = Kodein.Module {
    bind<GameRepository>() with singleton { GameRepositoryImpl() }

}