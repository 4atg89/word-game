package com.sk4atg89.words

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sk4atg89.words.screen.game.GameFragment
import com.sk4atg89.words.screen.game.model.AppGameSize
import com.sk4atg89.words.screen.game.model.AppLevel
import com.sk4atg89.words.screen.main.MainFragment
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

class MainActivity : AppCompatActivity(), KodeinAware {

    private val parentKodein by closestKodein()

    override val kodein = Kodein.lazy {
        extend(parentKodein)
//        import(activityModule())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val game = MainFragment.main()
        supportFragmentManager.beginTransaction()
            .add(R.id.container, game, game::class.java.name)
            .commit()
    }
}
