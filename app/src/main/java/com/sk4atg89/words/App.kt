package com.sk4atg89.words

import android.app.Application
import android.content.Context
import com.sk4atg89.words.di.applyModules
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.androidModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

class App : Application(), KodeinAware {

    override val kodein: Kodein = Kodein.lazy {
        import(androidModule(this@App))
        bind<Context>() with provider { this@App }
//        bind<ForegroundAppDetector>() with eagerSingleton {
//            ForegroundAppDetectorImpl().apply {
//                ProcessLifecycleOwner.get().lifecycle.addObserver(this@apply)
//            }
//        }
        applyModules(this)
    }

}