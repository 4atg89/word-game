package com.sk4atg89.words.screen.game

import android.os.Bundle
import android.view.View
import com.sk4atg89.words.R
import com.sk4atg89.words.screen.base.mv.BaseFragment
import com.sk4atg89.words.screen.base.mv.addObserver
import com.sk4atg89.words.screen.common.bundleOf
import com.sk4atg89.words.screen.common.getInt
import com.sk4atg89.words.screen.common.int
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GameFragment : BaseFragment<GameViewModel>(R.layout.fragment_game) {

    override val viewModel by instance<GameViewModel>()

    override val controllerModule: Kodein.Module = GameModule()

    private val delegate = GameDelegate().also(::addToLifecycle)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addObserver(delegate::render).also { viewModel.level(arguments!!.getInt()!!) }
    }

    companion object {

        private fun instance(bundle: Bundle) = GameFragment().apply { arguments = bundle }

        fun game(level: Int) = bundleOf { int(level) }.let(::instance)

    }

}