package com.sk4atg89.words.screen.settings

import com.sk4atg89.words.screen.base.mv.BaseViewModel
import com.sk4atg89.words.screen.base.mv.BaseViewModelImpl

interface SettingsViewModel : BaseViewModel {
}

class SettingsViewModelImpl : BaseViewModelImpl(), SettingsViewModel {
}