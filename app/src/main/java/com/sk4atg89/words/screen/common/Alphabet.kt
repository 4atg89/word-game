package com.sk4atg89.words.screen.common

import android.content.Context
import java.util.*

//АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ
//ABCDEFGHIJKLMNOPQRSTUVWXYZ
enum class Alphabet(val letters: String) {
    ENGLISH("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
    //todo when have BD check Ё letter whether it should present or not
    RUSSIAN("АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ");

    fun letters(): List<String> = letters.toList().map { it.toString() }
}

enum class AlphabetGenerator(val locale: Locale, val firstLetter: Char, val lastLetter: Char) {

    ENGLISH(Locale("en"), 'a', 'z'),
    RUSSIAN(Locale("ru"), 'а', 'я');

}

fun Context.getAlphabet(alphabet: AlphabetGenerator): CharArray {
    val firstLetter = alphabet.firstLetter
    val alphabetSize = alphabet.lastLetter - firstLetter + 1
    return CharArray(alphabetSize) { (it + firstLetter.toInt()).toChar().toUpperCase() }
}