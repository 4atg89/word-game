package com.sk4atg89.words.screen.game.reducer

import com.sk4atg89.domain.game.interactor.GameUseCase
import com.sk4atg89.words.screen.base.redux.Action
import com.sk4atg89.words.screen.base.redux.ActionState
import com.sk4atg89.words.screen.base.redux.Redux
import com.sk4atg89.words.screen.base.redux.State
import com.sk4atg89.words.screen.game.model.AppGameSize
import com.sk4atg89.words.screen.game.model.AppLevel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicReference

interface GameReducer {
    fun state(stateListener: (GameState) -> Unit)
    fun process(gameAction: GameAction)
}

class GameReducerImpl(private val gameUseCase: GameUseCase) : GameReducer {

    private val state = AtomicReference<(GameState) -> Unit>()

    private val redux: Redux<GameAction, GameState> = Redux.create(GameState.Loading) {
        on<GameAction.GetAction> { load() }
        on<GameAction.LoadedAction> { loaded() }
    }

    private fun ActionState<GameAction.GetAction, GameState>.load(): GameState.Loading {
        action.scope.launch {
            state.get().invoke(GameState.Loading)
//            val level = gameUseCase.level(action.number)
//                .let { AppLevel(it.words.map { it.word }, AppGameSize(5, 4)) }
            delay(100)
            val level = AppLevel(listOf(
                    "a", "b", "c", "d", "e",
                    "f", "g", "h", "i", "j",
                    "k", "l", "m", "n", "o",
                    "p", "q", "r", "s", "t"
                ), AppGameSize(5, 4))
            process(GameAction.LoadedAction(level))
            state.get().invoke(GameState.OnLoaded(level))
        }
        return GameState.Loading
    }

    private fun ActionState<GameAction.LoadedAction, GameState>.loaded(): GameState.OnLoaded =
        GameState.OnLoaded(action.level)

    override fun process(gameAction: GameAction) =
        redux.process(gameAction)

    override fun state(stateListener: (GameState) -> Unit) {
        state.set(stateListener)
    }
}

sealed class GameState : State {
    object Loading : GameState()
    class OnLoaded(val level: AppLevel) : GameState()
    fun bla(){}
}

sealed class GameAction : Action {
    class GetAction(val number: Int, val scope: CoroutineScope) : GameAction()
    class LoadedAction(val level: AppLevel) : GameAction()
    class SubmitAction(val number: Int) : GameAction()
}