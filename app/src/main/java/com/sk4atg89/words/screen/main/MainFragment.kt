package com.sk4atg89.words.screen.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import com.sk4atg89.words.R
import com.sk4atg89.words.screen.base.lifecycle.click
import com.sk4atg89.words.screen.base.mv.BaseFragment
import com.sk4atg89.words.screen.common.bundleOf
import com.sk4atg89.words.screen.game.GameFragment
import com.sk4atg89.words.screen.settings.SettingsFragment
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.item_main_menu.view.*
import org.kodein.di.generic.instance

class MainFragment : BaseFragment<MainViewModel>(R.layout.fragment_main) {

    override val viewModel by instance<MainViewModel>()
    override val controllerModule = MainModule()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainContainer.addView(continueGame())
        mainContainer.addView(newGame())
        mainContainer.addView(adItem())
        mainContainer.addView(languageItem())
        mainContainer.addView(settingsItem())

    }

    private fun menuItem() = LayoutInflater.from(context)
        .inflate(R.layout.item_main_menu, mainContainer, false)

    private fun TextView.bind(@StringRes res: Int, listener: (View) -> Unit) {
        text = getString(res); click(scope) { listener.invoke(it) }
    }

    private fun settingsItem() = menuItem()
        .also { it.menu.bind(R.string.settings) { toSettings() } }

    private fun toSettings() = router?.push(SettingsFragment.settings())

    private fun adItem() = menuItem()
        .also { it.menu.bind(R.string.ad) { adClick() } }

    private fun adClick() = Toast.makeText(context, " ad will be soon ", Toast.LENGTH_SHORT).show()

    private fun languageItem() = menuItem()
        .also { it.menu.bind(R.string.language) { languageClick() } }

    private fun languageClick() =
        Toast.makeText(context, " language will be soon ", Toast.LENGTH_SHORT).show()

    private fun continueGame() = menuItem()
        .also { it.menu.bind(R.string.continue_game) { toContinueGame() } }

    private fun toContinueGame() =
        Toast.makeText(context, " continue will be soon ", Toast.LENGTH_SHORT).show()

    private fun newGame() = menuItem()
        .also { it.menu.bind(R.string.start_new_game) { toNewGame() } }

    private fun toNewGame() = router?.push(GameFragment.game(4))

    companion object {

        private fun instance(bundle: Bundle) = MainFragment().apply { arguments = bundle }

        fun main() = bundleOf {  }.let(::instance)

    }

}