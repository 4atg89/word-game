package com.sk4atg89.words.screen.base.redux

interface Reducer<in ACTION : Action, STATE : State> {

    fun reduce(action: ACTION, state: STATE, onStateChanged: (state: STATE) -> Unit)

}

class BlockingReducer<ACTION : Action, STATE : State>(
    private val transformerFunction: ActionState<ACTION, STATE>.() -> STATE
) : Reducer<ACTION, STATE> {

    override fun reduce(action: ACTION, state: STATE, onStateChanged: (state: STATE) -> Unit) {
        onStateChanged(getState(action, state))
    }

    fun getState(action: ACTION, state: STATE) = transformerFunction(ActionState(action, state))
}
