package com.sk4atg89.words.screen.main

import com.sk4atg89.words.screen.base.mv.BaseViewModel
import com.sk4atg89.words.screen.base.mv.BaseViewModelImpl

interface MainViewModel : BaseViewModel {
}

class MainViewModelImpl : BaseViewModelImpl(), MainViewModel {

}