package com.sk4atg89.words.screen.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.sk4atg89.words.screen.base.lifecycle.ArchViewLifecycleProvider
import com.sk4atg89.words.screen.base.lifecycle.LifecycleScope
import com.sk4atg89.words.screen.base.lifecycle.ViewLifecycleProvider

abstract class FragmentCustomLifecycleOwner(container: Int) : Fragment(container) {

    private val lifecycleList = mutableListOf<FragmentLifecycle>()

    private val lifecycleProvider: ViewLifecycleProvider = ArchViewLifecycleProvider()

    private val lifecycleView = lifecycleProvider.lifecycle(this)

    protected val scope = LifecycleScope(lifecycleView)

    fun addToLifecycle(lifecycle: FragmentLifecycle) {
        if (lifecycleList.contains(lifecycle)) return
        lifecycleList.add(lifecycle)
    }

    fun removeLifecycleListener(lifecycle: FragmentLifecycle) =
        lifecycleList.remove(lifecycle)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        lifecycleList.forEach { it.attach(this, context) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleList.forEach { it.create(this) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleList.forEach { it.scope(scope) }
        lifecycleList.forEach { it.restoreState(this, savedInstanceState) }
        lifecycleList.forEach { it.viewCreated(this, view) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        lifecycleList.forEach { it.activityCreated(this) }
    }

    override fun onStart() {
        super.onStart()
        lifecycleList.forEach { it.start(this) }
    }

    override fun onResume() {
        super.onResume()
        lifecycleList.forEach { it.resume(this) }
    }

    override fun onPause() {
        super.onPause()
        lifecycleList.forEach { it.pause(this) }
    }

    override fun onStop() {
        super.onStop()
        lifecycleList.forEach { it.stop(this) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycleList.forEach { it.viewDestroyed(this) }
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycleList.forEach { it.destroy(this) }
    }

    override fun onDetach() {
        super.onDetach()
        lifecycleList.forEach { it.detach(this) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        lifecycleList.forEach { it.saveState(this, outState) }
    }

}