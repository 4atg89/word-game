package com.sk4atg89.words.screen.base.redux

data class ActionState<A : Action, S : State>(val action: A, val currentState: S)
