package com.sk4atg89.words.screen.settings

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider


object SettingsModule {
    operator fun invoke() = Kodein.Module {
        bind<SettingsViewModel>() with provider { SettingsViewModelImpl() }
    }
}