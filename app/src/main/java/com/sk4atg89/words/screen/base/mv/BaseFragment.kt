package com.sk4atg89.words.screen.base.mv

import android.os.Bundle
import android.view.View
import com.sk4atg89.words.screen.base.ControllerModuleHolder
import com.sk4atg89.words.screen.base.FragmentCustomLifecycleOwner
import com.sk4atg89.words.screen.base.extension.observe
import com.sk4atg89.words.screen.base.navigation.SimpleNavigator
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

abstract class BaseFragment<out VM : BaseViewModel>(container: Int) : FragmentCustomLifecycleOwner(container),
    KodeinAware, ControllerModuleHolder, SimpleNavigator {

    abstract val viewModel: VM

    protected val router get() = fragmentManager

    override val kodein: Kodein = Kodein.lazy {
        val parentKodein by closestKodein(requireNotNull(activity))
        extend(parentKodein, allowOverride = true)
        import(controllerModule, allowOverride = true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onCreated(savedInstanceState)
    }

    override fun onDestroyView() {
        viewModel.onCleared()
        super.onDestroyView()
    }

    private fun subscribe() {

        observe(viewModel.errorsStream) {
            //TODO will be soon
        }

    }

}

inline fun <reified T> BaseFragment<*>.addObserver(noinline observer: StateObserver<T>) =
    viewModel.addObserver(T::class, observer)
