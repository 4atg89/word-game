package com.sk4atg89.words.screen.base.lifecycle

import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import com.sk4atg89.words.screen.base.FragmentLifecycle

class AndroidAttachDetachLifecycle : FragmentLifecycle, LifecycleOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    override fun viewCreated(fragment: Fragment, view: View) =
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)

    override fun start(fragment: Fragment) =
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START)

    override fun resume(fragment: Fragment) =
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

    override fun pause(fragment: Fragment) =
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)

    override fun stop(fragment: Fragment) =
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)

    override fun viewDestroyed(fragment: Fragment) =
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)

    override fun getLifecycle(): Lifecycle =
        lifecycleRegistry

}