package com.sk4atg89.words.screen.base.lifecycle

import androidx.lifecycle.Lifecycle
import com.sk4atg89.words.screen.base.FragmentCustomLifecycleOwner


interface ViewLifecycleProvider {

    fun lifecycle(fragment: FragmentCustomLifecycleOwner): Lifecycle

}

class ArchViewLifecycleProvider : ViewLifecycleProvider {

    override fun lifecycle(fragment: FragmentCustomLifecycleOwner): Lifecycle =
        AndroidAttachDetachLifecycle().apply(fragment::addToLifecycle).lifecycle

}