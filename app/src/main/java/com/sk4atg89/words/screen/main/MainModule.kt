package com.sk4atg89.words.screen.main

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

object MainModule {
    operator fun invoke() = Kodein.Module {
        bind<MainViewModel>() with provider { MainViewModelImpl() }
    }
}