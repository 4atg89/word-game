package com.sk4atg89.words.screen.base.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.sk4atg89.words.R

//const val TRANSACTION_DURATION = 150L

interface SimpleNavigator : BackNavigator/*, ParentNavigator, ChildNavigator */ {

    fun FragmentManager.push(fragment: Fragment, animate: Boolean = true) = withBackStack(fragment, animate)

    private fun FragmentManager.replace(fragment: Fragment, animate: Boolean = true) = noBackStack(fragment, animate)

    private fun FragmentManager.withBackStack(fragment: Fragment, animate: Boolean = true) {
        val current = fragments.find { it.isVisible } ?: return
        beginTransaction()
            .animateIfNeeded(animate)
            .addToBackStack(fragment::class.java.name)
            .replace(current.id, fragment, fragment::class.java.name)
            .commit()
    }
    // todo refactor
    private fun FragmentManager.noBackStack(fragment: Fragment, animate: Boolean = true) {
        val current = fragments.find { it.isVisible } ?: return
        beginTransaction()
            .animateIfNeeded(animate)
            .replace(current.id, fragment, fragment::class.java.name)
            .commit()
    }

    private fun FragmentTransaction.animateIfNeeded(animate: Boolean) = apply {
        if (animate) setCustomAnimations(
            R.anim.enter_from_right, R.anim.exit_to_left,
            R.anim.enter_from_left, R.anim.exit_to_right
        )
    }


//    fun Router.go(controller: Controller, animate: Boolean = true) = launchOnUi { replaceTopController(transaction(controller, animate)) }
//
//    fun Router.to(controller: Controller, animate: Boolean = true) = launchOnUi { pushController(transaction(controller, animate)) }
//
//    fun Router.single(controller: Controller, animate: Boolean = false) = launchOnUi { setRoot(transaction(controller, animate)) }
//
//    fun Router.shared(controller: Controller) = launchOnUi { pushController(animateShared(controller)) }
//
//    fun Router.pop() = launchOnUi { popCurrentController() }
//
//    fun Router.isRoot() = backstackSize == 1

//    private fun transaction(controller: Controller, animate: Boolean = true) = when {
//        animate -> animateTransaction(controller)
//        animate.not() -> defaultTransaction(controller)
//        else -> throw IllegalArgumentException("ignore")
//    }
//
//    private fun defaultTransaction(controller: Controller) = RouterTransaction.with(controller).tag(controller.javaClass.name)
//
//    private fun animateTransaction(controller: Controller) = RouterTransaction.with(controller)
//            .popChangeHandler(HorizontalChangeHandler(TRANSACTION_DURATION))
//            .pushChangeHandler(HorizontalChangeHandler(TRANSACTION_DURATION))
//            .tag(controller.javaClass.name)
//
//    private fun animateShared(controller: Controller) = RouterTransaction.with(controller)
//            .popChangeHandler(SharedChangeHandler())
//            .pushChangeHandler(SharedChangeHandler())
//            .tag(controller.javaClass.name)
}

//inline fun <reified T : Controller> Router.popTo()= popToTag(T::class.java.name, HorizontalChangeHandler(TRANSACTION_DURATION))
//
//inline fun <reified T : Controller> Router.findAttached() = backstack.map { it.controller() }.find { it is T && it.isAttached } as T?
//
//inline fun <reified T : Controller> Router.find()= backstack.map { it.controller() }.filterIsInstance<T>().firstOrNull()