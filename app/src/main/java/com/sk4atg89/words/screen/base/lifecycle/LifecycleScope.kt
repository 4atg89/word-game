package com.sk4atg89.words.screen.base.lifecycle

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancelChildren

typealias ScopeObserver = () -> Unit

class LifecycleScope(lifecycle: Lifecycle, context: CoroutineScope = MainScope()) :
    CoroutineScope by context, LifecycleObserver {

    private val observers = mutableListOf<ScopeObserver>()
    private var lifecycle = false
    val isAlive get() = lifecycle
    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onResume() {
        lifecycle = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onPause() {
        coroutineContext.apply { cancelChildren() }
        lifecycle = false
        observers.apply { forEach { it.invoke() } }.clear()
    }

    fun addObserver(observer: ScopeObserver) = observers.add(observer)

    fun removeObserver(observer: ScopeObserver) = observers.remove(observer)

    fun clearObservers() = observers.clear()

}