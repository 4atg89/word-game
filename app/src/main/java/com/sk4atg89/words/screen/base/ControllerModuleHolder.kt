package com.sk4atg89.words.screen.base

import org.kodein.di.Kodein

interface ControllerModuleHolder {

    val controllerModule: Kodein.Module
}