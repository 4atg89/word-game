package com.sk4atg89.words.screen.base.mv

import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.sk4atg89.words.screen.base.FragmentCustomLifecycleOwner
import com.sk4atg89.words.screen.base.FragmentLifecycle
import com.sk4atg89.words.screen.base.lifecycle.LifecycleScope
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.*
import ru.ldralighieri.corbind.view.clicks
import java.util.concurrent.atomic.AtomicReference

open class ViewDelegate : FragmentLifecycle, LayoutContainer {

    private val viewRef = AtomicReference<View>()

    protected val fragmentRef = AtomicReference<FragmentCustomLifecycleOwner>()

    protected val scopeRef = AtomicReference<LifecycleScope>()

    protected val scope get() = scopeRef.get()

    override val containerView: View? get() = viewRef.get()

    override fun create(fragment: Fragment) {
        super.create(fragment)
        fragmentRef.set(fragment as FragmentCustomLifecycleOwner)
    }

    override fun scope(scope: LifecycleScope) {
        super.scope(scope)
        scopeRef.set(scope)
    }

    override fun viewCreated(fragment: Fragment, view: View) {
        super.viewCreated(fragment, view)
        viewRef.set(view)
        view.clicks()
    }

    override fun viewDestroyed(fragment: Fragment) {
        super.viewDestroyed(fragment)
        scopeRef.set(null)
        viewRef.set(null)
        clearFindViewByIdCache()
    }

    override fun destroy(fragment: Fragment) {
        super.destroy(fragment)
        fragmentRef.set(null)
    }

    protected val context: Context get() = viewRef.get().context

    internal fun <T : View> findById(id: Int) = viewRef.get()?.findViewById<T>(id)

}
