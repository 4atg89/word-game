package com.sk4atg89.words.screen.game.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class AppGameSize(val column: Int, val row: Int) : Parcelable {

    val size = column * row
}