package com.sk4atg89.words.screen.base.mv

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.lifecycle.*
import com.sk4atg89.words.screen.base.SingleLiveEvent
import kotlinx.coroutines.*
import timber.log.Timber
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

typealias StateObserver<T> = (T) -> Unit

interface BaseViewModel : CoroutineScope {

    val errorsStream: LiveData<Throwable>

    fun onCreated(savedInstanceState: Bundle?)

    fun <T> addObserver(clazz: KClass<*>, observer: StateObserver<T>)

    fun onCleared()
}

abstract class BaseViewModelImpl : BaseViewModel, ViewModel() {

    val observers = mutableMapOf<KClass<*>, MutableList<StateObserver<*>>>()

    private val handler = CoroutineExceptionHandler { _, exception ->
        errorsStream.postValue(exception)
    }

    private val job = SupervisorJob()

    override val coroutineContext: CoroutineContext get() = job + Dispatchers.Main + handler

    override val errorsStream = SingleLiveEvent<Throwable>()

    override fun onCreated(savedInstanceState: Bundle?) {}

    @Suppress("UNCHECKED_CAST")
    inline fun <reified T> notifier(value: T) {
        val list = observers[T::class] as? MutableList<StateObserver<T>>
        if (list.isNullOrEmpty()) Timber.d("!!!!!!!!!!!!!!!!!there is no notifier!!!!!!!!!!!!!!!!!")
        list?.forEach { it.invoke(value) }
    }

    override fun <T> addObserver(clazz: KClass<*>, observer: StateObserver<T>) {
        if (observers[clazz] == null) observers[clazz] = mutableListOf()
        //todo check if (contains(observer).not()) works correctly
        with(observers[clazz] ?: return) { if (contains(observer).not()) add(observer) }
    }

    @CallSuper
    override fun onCleared() {
        job.cancel()
        observers.clear()
        super.onCleared()
    }

    protected fun launchOn(block: suspend CoroutineScope.() -> Unit): Job = launch(block = block)

}