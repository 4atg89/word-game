package com.sk4atg89.words.screen.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.sk4atg89.words.screen.base.lifecycle.LifecycleScope

interface FragmentLifecycle {

    fun attach(fragment: Fragment, context: Context) {}

    fun create(fragment: Fragment) {}

    fun viewCreated(fragment: Fragment, view: View) {}

    fun scope(scope: LifecycleScope) {}

    fun start(fragment: Fragment) {}

    fun resume(fragment: Fragment) {}

    fun pause(fragment: Fragment) {}

    fun stop(fragment: Fragment) {}

    fun viewDestroyed(fragment: Fragment) {}

    fun activityCreated(fragment: Fragment) {}

    fun destroy(fragment: Fragment) {}

    fun detach(fragment: Fragment) {}

    fun saveState(fragment: Fragment, outState: Bundle) {}

    fun restoreState(fragment: Fragment, outState: Bundle?) {}

}