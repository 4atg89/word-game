package com.sk4atg89.words.screen.base.redux

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

class Redux<ACTION : Action, STATE : State>(initialState: STATE) {

    private val actionStateMap = mutableMapOf<KClass<out ACTION>, Reducer<ACTION, STATE>>()

    private var currentState: STATE = initialState

    fun process(action: ACTION) {
        if (actionStateMap.isEmpty()) {
            throw IllegalStateException("No actions can be processed. Kaskade might be unsubscribed at this point.")
        }

        getReducer(action)?.let { reducer ->
            reducer.reduce(action, currentState) { state ->
//                if (state !is SingleEvent) {
                    currentState = state
//                } else {
//                    emitOrEnqueueState(state)
//                }
            }
        }// ?: throw IncompleteFlowException(action)
    }


    @Suppress("UNCHECKED_CAST")
    internal fun <T : ACTION> getReducer(action: T): Reducer<T, STATE>? =
        actionStateMap[action::class] as? Reducer<T, STATE>

    fun addActions(builder: Builder<ACTION, STATE>.() -> Unit) {
        val eventBuilder = Builder<ACTION, STATE>()
        eventBuilder.builder()
        actionStateMap.putAll(eventBuilder.transformer)
    }

    class Builder<ACTION : Action, STATE : State> internal constructor() {

        private val _transformerMap = mutableMapOf<KClass<out ACTION>, Reducer<ACTION, STATE>>()

        internal val transformer get() = _transformerMap.toMap()

        inline fun <reified T : ACTION> on(noinline transformer: ActionState<T, STATE>.() -> STATE) {
            on(T::class, BlockingReducer(transformer))
        }

//        inline fun <reified T : ACTION> CoroutineScope.on(noinline transformer: suspend ActionState<T, STATE>.() -> STATE) {
//            on(T::class, ScopedReducer(transformer))
//        }
//
//        inline fun <reified T : ACTION> on(callback: (T) -> Unit) {
//
//        }

        @Suppress("UNCHECKED_CAST")
        fun <T : ACTION> on(clazz: KClass<T>, reducer: Reducer<T, STATE>) {
            _transformerMap[clazz] = reducer as Reducer<ACTION, STATE>
        }
    }

    companion object {

        fun <ACTION : Action, STATE : State> create(
            initialState: STATE, builder: Builder<ACTION, STATE>.() -> Unit
        ): Redux<ACTION, STATE> {
            val kaskade = Redux<ACTION, STATE>(initialState)
            kaskade.addActions(builder)
            return kaskade
        }
    }

}