package com.sk4atg89.words.screen.settings

import android.os.Bundle
import com.sk4atg89.words.R
import com.sk4atg89.words.screen.base.mv.BaseFragment
import com.sk4atg89.words.screen.common.bundleOf
import org.kodein.di.generic.instance

class SettingsFragment : BaseFragment<SettingsViewModel>(R.layout.fragment_settings) {

    override val viewModel by instance<SettingsViewModel>()
    override val controllerModule = SettingsModule()

    companion object {

        private fun instance(bundle: Bundle) = SettingsFragment().apply { arguments = bundle }

        fun settings() = bundleOf {  }.let(::instance)

    }

}