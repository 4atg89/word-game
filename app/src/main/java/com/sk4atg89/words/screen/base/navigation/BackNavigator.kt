package com.sk4atg89.words.screen.base.navigation

import androidx.fragment.app.FragmentManager

interface BackNavigator {

    fun FragmentManager.back() = popBackStack()

}