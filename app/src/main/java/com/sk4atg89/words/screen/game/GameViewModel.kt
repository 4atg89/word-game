package com.sk4atg89.words.screen.game

import com.sk4atg89.words.screen.base.mv.BaseViewModel
import com.sk4atg89.words.screen.base.mv.BaseViewModelImpl
import com.sk4atg89.words.screen.game.reducer.GameAction
import com.sk4atg89.words.screen.game.reducer.GameReducer

interface GameViewModel : BaseViewModel {

    fun level(number: Int)

}

class GameViewModelImpl(private val gameReducer: GameReducer) : GameViewModel, BaseViewModelImpl() {

    override fun level(number: Int) = gameReducer
        .apply { state(::notifier) }.process(GameAction.GetAction(number, this))

}
