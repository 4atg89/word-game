package com.sk4atg89.words.screen.base.lifecycle

import android.view.View
import androidx.annotation.CheckResult
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.isActive

fun View.click(scope: LifecycleScope, listener: (View) -> Unit) =
    onClick(scope).filter { scope.isAlive }.drop(1).take(1).onEach { listener.invoke(this) }.launchIn(scope)

fun View.clicks(scope: LifecycleScope, listener: (View) -> Unit) =
    onClick(scope).filter { scope.isAlive }.drop(1).onEach { listener.invoke(this) }.launchIn(scope)

fun View.onClick(scope: LifecycleScope): Flow<Int> = MutableStateFlow(0).apply {
    setOnClickListener(listener(scope) { value++ })
    scope.addObserver { setOnClickListener(null) }
}

@CheckResult
private fun listener(scope: LifecycleScope, emitter: () -> Unit) = View.OnClickListener {
    if (scope.isActive) { emitter() }
}
