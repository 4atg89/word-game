package com.sk4atg89.words.screen.game

import android.content.Context
import android.graphics.Point
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.sk4atg89.words.R
import com.sk4atg89.words.screen.base.lifecycle.clicks
import com.sk4atg89.words.screen.base.mv.ViewDelegate
import com.sk4atg89.words.screen.common.getDimens
import com.sk4atg89.words.screen.game.model.AppLevel
import com.sk4atg89.words.screen.game.reducer.GameState
import com.sk4atg89.words.screen.widget.CellView
import kotlinx.android.synthetic.main.fragment_game.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn

class GameDelegate() : ViewDelegate() {

    private var cellMargin = -1

    override fun viewCreated(fragment: Fragment, view: View) {
        super.viewCreated(fragment, view)
        cellMargin = view.context.getDimens(R.dimen.cell_margin)

    }

    private fun onLevel(level: AppLevel) {
        val gameSize = level.gameSize
        val marginScreen = context.getDimens(R.dimen.game_margin) + gameSize.column * cellMargin
        val width = Point().also { fragmentRef.get()?.activity?.windowManager?.defaultDisplay?.getSize(it) }
            .run { (x - marginScreen * 2) / gameSize.column }

        level.letters.mapIndexed { index: Int, _: String -> index }.forEach {
            level.addViewToGame(it, width)
        }
    }

    private fun AppLevel.addViewToGame(position: Int, viewSize: Int) {
        if (position % gameSize.column == 0) container.addView(LinearLayout(context))
        val row = container.getChildAt(position / gameSize.column) as LinearLayout
        val cellView = row.context.createCell(viewSize).apply { cellPosition = position }
        cellView.clicks(scope) {
            val a = it as CellView
            Toast.makeText(context, "a = ${a.cellPosition} and ${a.letter}", Toast.LENGTH_SHORT).show()
        }
        cellView.letter = letters[position]
        row.addView(cellView)

    }

    //testing crash on views clicks
    fun test(view: View) = flow<Int> {
        var i = 0
        while (true) {
            i = i.inc()
            delay(20)
            view.callOnClick()
        }
    }.launchIn(scope)

    private fun Context.createCell(size: Int) = CellView(this).apply {
        layoutParams = LinearLayout.LayoutParams(size, size)
            .apply { setMargins(cellMargin, cellMargin, cellMargin, cellMargin) }
    }


    fun render(state: GameState) {
        when(state){
            is GameState.OnLoaded -> onLevel(state.level)
        }
    }

}