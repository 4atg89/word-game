package com.sk4atg89.words.screen.base

import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel
import java.util.concurrent.ConcurrentLinkedQueue

interface SubscriptionContainer {
    /**
     * Adds job to container
     * @param job - coroutine job; [Job]
     * @return itself
     * */
    fun addJob(job: JobPair): Job

    /**
     * Removes job from container
     * @param job - coroutine job; [Job]
     * @return itself
     * */
    fun removeJob(job: JobPair): Job

    /**
     * Clears job container
     * */
    fun cancelAllJobs()

    /**
     * Returns jobs to be retried
     */
    fun jobsToRetry(): List<() -> Job>

    /**
     * Overloads plus operator to add job into container
     * @param job - [Job]
     * */
    operator fun plusAssign(job: JobPair)

    /**
     * Overloads minus operator to remove job from container
     * @param job - [Job]
     * */
    operator fun minusAssign(job: JobPair)


    /**
     * Adds subscription to container
     * @param subscription - coroutine job; [SubscriptionReceiveChannel]
     * @return itself
     * */
    fun <T> addSubscription(
            subscription: ReceiveChannel<T>): ReceiveChannel<T>

    /**
     * Removes subscription from container
     * @param subscription - coroutine job; [SubscriptionReceiveChannel]
     * @return itself
     * */
    fun <T> removeSubscription(subscription: ReceiveChannel<T>): ReceiveChannel<T>

    /**
     * Clears channels container
     * */
    fun cancelAllSubscriptions()

    /**
     * Overloads plus operator to add subscription into container
     * @param subscription - [SubscriptionReceiveChannel]
     * */
    operator fun <T> plusAssign(subscription: ReceiveChannel<T>)

    /**
     * Overloads minus operator to remove subscription from container
     * @param subscription - [SubscriptionReceiveChannel]
     * */
    operator fun <T> minusAssign(subscription: ReceiveChannel<T>)

    /**
     * Clears channels and jobs containers
     * */
    fun cancelAll()
}

/**
 * Simple [SubscriptionContainer] implementation
 */
class SubscriptionsContainerDelegate : SubscriptionContainer {

    private val jobs = ConcurrentLinkedQueue<JobPair>()
    private val subscriptions = ConcurrentLinkedQueue<ReceiveChannel<*>>()

    override fun addJob(job: JobPair): Job = job.also { jobs.add(it) }.job

    override fun removeJob(job: JobPair): Job = job.also { jobs.remove(it) }.job

    override fun jobsToRetry(): List<() -> Job> {
        // filter failed jobs
        val toRetry =
                listOf(*jobs.toTypedArray())
                        .filter { it.job.isCompleted || it.job.isCancelled }
                        .map { it.block }

        // clear current job list
        jobs.clear()

        return toRetry
    }

    override fun cancelAllJobs() {
        jobs.forEach { it.job.cancel() }
        jobs.clear()
    }

    override fun plusAssign(job: JobPair) {
        addJob(job)
    }

    override fun minusAssign(job: JobPair) {
        removeJob(job)
    }

    override fun <T> addSubscription(subscription: ReceiveChannel<T>)
            : ReceiveChannel<T> = subscription.also { subscriptions.add(it) }


    override fun <T> removeSubscription(subscription: ReceiveChannel<T>)
            : ReceiveChannel<T> = subscription.also { subscriptions.remove(it) }

    override fun cancelAllSubscriptions() {
        subscriptions.forEach { it.cancel() }
        subscriptions.clear()
    }

    override fun <T> plusAssign(subscription: ReceiveChannel<T>) {
        addSubscription(subscription)
    }


    override fun <T> minusAssign(subscription: ReceiveChannel<T>) {
        removeSubscription(subscription)
    }

    override fun cancelAll() {
        cancelAllJobs()
        cancelAllSubscriptions()
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Extension to add Pair<Job, () -> Job> into container
 * pair.second is used to rerun job in case of failure
 * @param subscriptionContainer - [SubscriptionContainer]
 * @return [Job]
 */
fun JobPair.bind(subscriptionContainer: SubscriptionContainer): JobPair {
    subscriptionContainer += this
    return this
}

fun <T> ReceiveChannel<T>.bind(subscriptionContainer: SubscriptionContainer): ReceiveChannel<T> {
    subscriptionContainer += this
    return this
}

/**
 * Creates coroutine, which will be automatically added to the subscription container
 */

fun <T> BroadcastChannel<T>.openAndBind(subscriptionContainer: SubscriptionContainer): ReceiveChannel<T> {
    return this.openSubscription().also { it.bind(subscriptionContainer) }
}

data class JobPair(
        val job: Job,
        val block: () -> Job
)

infix fun Job.combine(block: () -> Job) = JobPair(this, block)
