package com.sk4atg89.words.screen.widget

import android.content.Context
import android.util.AttributeSet
import androidx.cardview.widget.CardView
import com.sk4atg89.words.screen.common.colorState
import com.sk4atg89.words.R
import com.sk4atg89.words.screen.common.getDimens
import kotlinx.android.synthetic.main.view_cell.view.*

class CellView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    init {
        radius = context.getDimens(R.dimen.cell_radius).toFloat()
        inflate(context, R.layout.view_cell, this) }

    var cellPosition = -1

    var letter: String
        get() = tvCell.text.toString()
        set(value) {
            tvCell.text = value.toUpperCase()
            setCardBackgroundColor(context.colorState(R.drawable.cell_item_background))
        }

}