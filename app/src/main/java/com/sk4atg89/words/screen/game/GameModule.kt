package com.sk4atg89.words.screen.game

import com.sk4atg89.words.screen.game.reducer.GameReducer
import com.sk4atg89.words.screen.game.reducer.GameReducerImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

object GameModule {
    operator fun invoke() = Kodein.Module {
        bind<GameReducer>() with provider { GameReducerImpl(instance()) }
        bind<GameViewModel>() with provider { GameViewModelImpl(instance()) }
    }
}