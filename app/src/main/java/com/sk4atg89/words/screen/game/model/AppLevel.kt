package com.sk4atg89.words.screen.game.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AppLevel(val letters: List<String>, val gameSize: AppGameSize) : Parcelable