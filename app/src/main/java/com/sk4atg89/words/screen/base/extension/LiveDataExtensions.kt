package com.sk4atg89.words.screen.base.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, func: (T) -> Unit) {
    liveData.observe(this, Observer { it?.let(func) })
}

inline fun <reified T: Any?> LiveData<T>.withValue(valueJob:(T) -> Unit) {
    this.value?.let(valueJob)
}
