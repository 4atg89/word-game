package com.sk4atg89.words.di.initializer

import android.app.Application
import com.sk4atg89.data.AppInfoProvider
import timber.log.Timber
import java.util.*

typealias Initializer = (app: Application) -> Unit

//fun stethoInitializer(appInfo: AppInfoProvider): Initializer = {
//    if (appInfo.isDebug) {
//        Stetho.initializeWithDefaults(it)
//    }
//}
//
//fun fabricInitializer(appInfo: AppInfoProvider): Initializer = {
//    if (!appInfo.isDebug) {
//        Fabric.with(it, Crashlytics())
//    }
//}

fun timberInitializer(appInfo: AppInfoProvider): Initializer = {
    if (appInfo.isDebug) {
        Timber.plant(Timber.DebugTree())
    } //else {
//        Timber.plant(CrashReportingTree())
//    }
}

//fun loggerInitializer(logProcessor: LogProcessor): Initializer = {
//    Logger.invoke(logProcessor)
//}

