package com.sk4atg89.words.di

import com.sk4atg89.data.AppInfoProvider
import com.sk4atg89.data.di.dataModules
import com.sk4atg89.domain.di.domainModules
import com.sk4atg89.words.AppInfoProviderImpl
import com.sk4atg89.words.di.initializer.Initializer
import com.sk4atg89.words.di.initializer.timberInitializer
import org.kodein.di.Kodein
import org.kodein.di.generic.*

fun applyModules(builder: Kodein.Builder) =
        builder.apply {
            import(appModule)
//            import(coreModules)
            import(dataModules)
            import(domainModules)
        }

private val appModule = Kodein.Module {

//    import(resourceProvidersModule)
//    import(converterModule)
    import(initializersModule)
    import(appInfoModule)
//    import(languageModule)
//    import(regionModule)
//    import(logModule)
//    import(uiModule)
//    import(coroutinesModule)
//    import(convertersModule)
//    import(permissionsModule)
//    import(googleServicesModule)
//    import(jumpersModule)
//    import(navigatorsModules)
//    import(pushNotificationModule)
//    import(errorHandlersModule)
//    import(dataTrackerModule)
//    import(migrationModules)
//    import(AnalyticsModule())
}
//
//private val resourceProvidersModule = Kodein.Module {
//    bind<VideoProvider>() with singleton { VideoProviderImpl(instance()) }
//    bind<TermsConditionsProvider>() with singleton { TermsConditionsProviderImpl(instance()) }
//}
//
//private val converterModule = Kodein.Module {
//    bind<DateConverter>() with singleton { DateConverter(instance()) }
//    bind<ReactionsFormatter>() with singleton { SimpleReactionsFormatter }
//}
//
//private val errorHandlersModule = Kodein.Module {
//
//    bind<ExceptionHandler>() with singleton { NetworkExceptionHandler(instance(), instance()) }
//    bind<CoroutineContext>() with singleton { WORKER_POOL + instance<CoroutineExceptionHandler>() }
//    bind<CoroutineExceptionHandler>() with singleton {
//        val networkExceptionProcessor = NetworkExceptionProcessor()
//        val analyticsService = instance<AnalyticsService>()
//
//        CoroutineExceptionHandler { _, e ->
//            // log error
//            e.message?.let { Logger.d(it, e) }
//
//            val processedError = networkExceptionProcessor.process(e)
//            processedError?.let {
//                analyticsService.send(ActionServerError(processedError))
//            }
//            // process error & send event to analytics
//        }
//    }
//}
//
//private val dataTrackerModule = Kodein.Module {
//    bind<DataTracker>() with singleton { DataTrackerImpl(instance()) }
//}
//
private val initializersModule = Kodein.Module {
//
    bind() from setBinding<Initializer>()
//    bind<Initializer>().inSet() with singleton { stethoInitializer(instance()) }
//    bind<Initializer>().inSet() with singleton { fabricInitializer(instance()) }
    bind<Initializer>().inSet() with singleton { timberInitializer(instance()) }
//    bind<Initializer>().inSet() with singleton { loggerInitializer(instance()) }
//    bind<Initializer>().inSet() with singleton { languageInitializer(instance(), instance(), instance()) }
//    bind<Initializer>().inSet() with singleton { analyticsInitializer(instance()) }
//    bind<Initializer>().inSet() with singleton { placesInitializer() }
//    bind<Initializer>().inSet() with singleton { versionInitializer(instance(), instance(), instance()) }
//    bind<Initializer>().inSet() with singleton { firebaseInitializer() }
//    bind<Initializer>().inSet() with singleton { jumperInitializer(instance()) }
}
//
private val appInfoModule = Kodein.Module {
    bind<AppInfoProvider>() with singleton { AppInfoProviderImpl(instance()) }
}
//
//private val languageModule = Kodein.Module {
//    bind<LanguageProvider>() with singleton { LanguageProviderImpl() }
//    bind<AppLanguageRefresher>() with singleton { AppLanguageRefresherImpl(instance(), instance(), instance()) }
//}
//
//private val regionModule = Kodein.Module {
//    bind<AppRegionRefresher>() with singleton { AppRegionRefresherImpl(instance()) }
//}
//
//private val logModule = Kodein.Module {
//    bind<LogProcessor>() with singleton { LogProcessorImpl() }
//}
//
//private val uiModule = Kodein.Module {
//    bind<LocationsDelegate>() with provider { LocationsDelegateImpl() }
//    bind<DialogHelper>() with provider { DialogHelper() }
//    bind<DiffCallbackFactory>() with singleton { DiffCallbackFactoryImpl() }
//}
//
//private val coroutinesModule = Kodein.Module {
//    bind<CoroutineCancellationHandler>() with singleton { CoroutineCancellationHandler() }
//}
//
//private val convertersModule = Kodein.Module {
//    bind<ConverterBindings>() with singleton {
//        object : ConverterBindings {
//            override val converters: List<ConvertersContextRegistrationCallback> =
//                    listOf(
//                            CommonAppEntitiesConverter(),
//                            CommonAppProfileConverter(instance()),
//                            CommonAppSettingsConverter(),
//                            CommonAppCompetitionsConverter(),
//                            CommonAppRegionConverter(),
//                            CommonAppBattleConverter(),
//                            CommonAppClansConverter(),
//                            CommonAppTeamConverter(),
//                            ProfilePresentationConverter(),
//                            LeaguesPresentationConverter(),
//                            TournamentGridPresentationConverter(),
//                            CommentsPresentationConverter(),
//                            MapsPresentationConverter(),
//                            MentionablePresentationsConverter(),
//                            LeaderboardConverter(),
//                            TournamentsPresentationConverter(),
//                            TournamentsDetailPresentationConverter(),
//                            CustomFieldsPresentationConverter(),
//                            LeaderboardConverter(),
//                            BattleConverter(),
//                            AppProLeaguesConverter(),
//                            RegionsConverter(),
//                            AppFeedConverter(),
//                            AppAttachConverter(),
//                            AppChatConverter(),
//                            AppChannelConverter(),
//                            AppPushConverter(),
//                            AppNotificationsConverter()
//                    )
//        }
//    }
//}
//
//private val googleServicesModule = Kodein.Module {
//    bind<PlayServiceHelper>() with singleton { PlayServiceHelper(instance()) }
//}
//
//private val permissionsModule = Kodein.Module {
//    bind<RevokedPermissionsDelegate>() with singleton {
//        RevokedPermissionsDelegateImpl(instance(), instance())
//    }
//}
//
//private val jumpersModule = Kodein.Module {
//    bind<AppJumper>() with singleton { AppJumperImpl() }
//    bind<AppSettingsJumper>() with singleton { AppSettingsJumperImpl(instance(), instance()) }
//    bind<AppLinkJumper>() with singleton { AppLinkJumperImpl(instance()) }
//    bind<AppMailJumper>() with singleton { AppMailJumper(instance()) }
//}
//
//private val navigatorsModules = Kodein.Module {
//    bind<SocialsParticipantNavigator>() with singleton { SocialsProfileNavigatorImpl(instance()) }
//    bind<LinkifyNavigator>() with provider { LinkifyNavigatorImpl(instance(), instance()) }
//}
//
//private val pushNotificationModule = Kodein.Module {
//    bind<NotificationAdapter>() with singleton { PushNotificationAdapter(instance(), instance()) }
//    bind<PushRegistrationProvider>() with singleton { PushRegistrationProviderImpl(instance(), instance()) }
//}
