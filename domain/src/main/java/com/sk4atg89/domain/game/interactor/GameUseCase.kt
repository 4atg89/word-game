package com.sk4atg89.domain.game.interactor

import com.sk4atg89.domain.game.model.Level
import com.sk4atg89.domain.game.repository.GameRepository

interface GameUseCase {

    suspend fun level(number: Int): Level

}

internal class GameUseCaseImpl(private val repository: GameRepository) : GameUseCase {

    override suspend fun level(number: Int): Level = repository.level(number)

}