package com.sk4atg89.domain.game.model

data class GameSize(val column: Int, val row: Int)