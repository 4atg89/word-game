package com.sk4atg89.domain.game.model

class Level(val number: Int, val words: List<Word>, val size: GameSize)