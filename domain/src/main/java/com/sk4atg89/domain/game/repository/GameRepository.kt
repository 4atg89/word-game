package com.sk4atg89.domain.game.repository

import com.sk4atg89.domain.game.model.Level

interface GameRepository {

    suspend fun level(number: Int): Level

}