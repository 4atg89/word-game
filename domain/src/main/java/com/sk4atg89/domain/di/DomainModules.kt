package com.sk4atg89.domain.di

import com.sk4atg89.domain.game.interactor.GameUseCase
import com.sk4atg89.domain.game.interactor.GameUseCaseImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider


val domainModules = Kodein.Module {
    import(useCasesModule)
//    import(otherModule)
}

private val useCasesModule = Kodein.Module {
    bind<GameUseCase>() with provider { GameUseCaseImpl(instance()) }
}


